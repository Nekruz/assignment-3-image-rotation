#include "include/img.h"

int rotate(struct image* img) {
  struct image* copy = NULL;
  copy = (struct image*) malloc(sizeof(struct image));
  if (copy == NULL) {
    fprintf(stderr, "Could not rotate image, not enough memory\n");
    return 1;
  }
  copy->height = img->height;
  copy->width = img->width;
  copy->data = (struct pixel*) malloc(sizeof(struct pixel) * img->width * img->height);
  if (copy == NULL) {
    fprintf(stderr, "Could not rotate image, not enough memory\n");
    return 1;
  }
  for (size_t i = 0; i < img->height; i++) {
    for (size_t j = 0; j < img->width; j++) {
      copy->data[i * copy->width + j] = img->data[i * copy->width + j];
    }
  }

  img->height = copy->width;
  img->width = copy->height;
  for (size_t i = 0; i < copy->height; i++) {
    for (size_t j = 0; j < copy->width; j++) {
      img->data[j * img->width + (img->width - 1 - i)] = copy->data[i * copy->width + j];
    }
  }
  destroy_img(copy);
  return 0;
}

void destroy_img(struct image* img) {
	if (img->data != NULL) {
    free(img->data);
  }
	if (img != NULL) {
    free(img);
  }
}
