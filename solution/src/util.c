#include "include/util.h"

char* getExtension(const char *name) {
  size_t size = 0;
  for (size = 0; name[size]; size++)
    ;
  size_t ans_iterator = 0;
  for (size_t i = size - 1; i > 0; i--) {
    if (name[i] == '.') {
      break;
    }
    ans_iterator++;
  }
  char *ans = (char *)malloc(sizeof(char) * (ans_iterator + 1));
  size_t j = ans_iterator - 1;
  for (size_t i = size - 1; i > 0; i--) {
    if (name[i] == '.') {
      break;
    }
    ans[j--] = name[i];
  }
  ans[ans_iterator] = 0;
  return ans;
}

enum image_type getType(const char *name) {
  char *ans = getExtension(name);
  if (strcmp(ans, "bmp") == 0 || strcmp(ans, "dib") == 0) {
    free(ans);
    return BMP;
  }
  free(ans);
  //Here more types can be supported
  return UNKNOWN_TYPE;
}

FILE* open_input_file(const char *inputFile) {
  FILE *in = NULL;
  in = fopen(inputFile, "rb");
  if (in == NULL) {
    fprintf(stderr, "Input file %s was not found\n", inputFile);
    return NULL;
  }
  return in;
}

struct compatibility_result check_compatibility(const char* filename) {
  struct compatibility_result ans =
      (struct compatibility_result){UNKNOWN_TYPE, COMP_OK, NULL};
  FILE* temp = NULL;
  temp = open_input_file(filename);
  if (temp == NULL) {
    ans.err = FILE_OPENING_ERROR;
    return ans;
  }
  ans.ptr = temp;
  enum image_type type = getType(filename);
  switch (type) {
  case BMP:
    ans.type = BMP;
    break;
  // Here more formats can be added
  default:
    fprintf(stderr, "The input image is not currently supported\n");
    ans = (struct compatibility_result){UNKNOWN_TYPE, NOT_SUPPORTED_TYPE, temp};
    break;
  }
  return ans;
}

enum process_result process(FILE *in, FILE *out, enum image_type type) {
  struct image* img = (struct image*) malloc(sizeof(struct image));
  enum read_status temp;
  switch (type)
  {
  case BMP:
    temp = read_bmp(in, img);
    if (temp == READ_OK) {
      int x = rotate(img);
      if (x) {
        return POINTER_ERROR;
      }
      enum write_status wr = write_bmp(out, img);
      if (wr != WRITE_OK) {
        break;
      }
    }
    break;
  // Add support for more types
  default:
    break;
  }
  if (img != NULL) {
    destroy_img(img);
  }
  return SUCCESS;
}
