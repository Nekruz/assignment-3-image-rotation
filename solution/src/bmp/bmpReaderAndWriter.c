#include "../include/bmpReaderAndWriter.h"

size_t get_padding(const struct image* img) {
    return (4 - (sizeof(struct pixel) * img->width) % 4) % 4;
}

enum read_status read_bmp(FILE* in, struct image* img) {
    fseek(in, 0, SEEK_SET);
    enum read_status temp;
    //Header is small enough to be saved in stack :)
    struct bmp_header header;
    temp = read_header(in, &header);
    if (temp != READ_OK) {
        return temp;
    }
    size_t x = fseek(in, header.bOffBits, SEEK_SET);
    //If image is corrupted here an error can occur
    if (x) {
        return READ_IMAGE_CORRUPTED;
    }
    img->height = header.biHeight;
    img->width = header.biWidth;
    img->data = NULL;
    img->data = (struct pixel*) malloc(sizeof(struct pixel) * img->height * img->width);
    if (img->data == NULL) {
        fprintf(stderr, "Image could not be read because you have too little available memory\n");
        return READ_OUT_OF_MEMORY;
    }
    temp = from_bmp(in, img);
    return temp;
}

enum read_status from_bmp( FILE* in, struct image* img) {
    size_t padding = get_padding(img);
    size_t x;
    unsigned char* cur = (unsigned char*)img->data;
    for (size_t i = 0; i < img->height; i++) {
        x = fread(cur, sizeof(struct pixel), img->width, in);
        if (x < img->width) {
            fprintf(stderr, "Incomplete image found\n");
            return READ_IMAGE_CORRUPTED;
        }
        //if image is corrupted an error can occur
        x = fseek(in, (long)padding, SEEK_CUR);
        if (x) {
            fprintf(stderr, "An unknown error when reading image occured\n");
            return READ_IMAGE_CORRUPTED;
        }
        cur += img->width * 3;
    }

    return READ_OK;
}

enum write_status write_bmp(FILE* out, const struct image* img) {
    fseek(out, 0, SEEK_SET);
    struct bmp_header new_header = build_header_from_img(img);
    enum write_status temp = write_header(out, &new_header);
    if (temp != WRITE_OK) {
        return temp;
    }
    temp = to_bmp(out, img);
    return temp;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    size_t padding = get_padding(img);
    size_t x;
    unsigned char* cur = (unsigned char*)img->data;
    for (size_t i = 0; i < img->height; i++) {
        x = fwrite(cur, sizeof(struct pixel), img->width, out);
        if (x < img->width) {
            fprintf(stderr, "Could not write some row to output file\n");
            return WRITE_ERROR;
        }
        cur += img->width * 3;
        if (padding) {
            x = fseek(out, (long)padding, SEEK_CUR);
            if (x) {
                fprintf(stderr, "Could not write some row to output file\n");
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}
