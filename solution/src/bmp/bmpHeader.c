#include "../include/bmpHeader.h"

size_t get_image_size(const struct image* img) {
    return img->height * (img->width * sizeof(struct pixel) + (4 - img->width%4) % 4);
}

struct bmp_header build_header_from_img(const struct image* img) {
    size_t sz = get_image_size(img);
    struct bmp_header ans = (struct bmp_header) {
        .bfType = SIGNATURE,
         .bfileSize = sizeof(struct bmp_header) + sz,
            .bfReserved = DEFAULT,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = HEADER_SIZE,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = DEFAULT,
            .biSizeImage = sz,
            .biXPelsPerMeter = DEFAULT,
            .biYPelsPerMeter = DEFAULT,
            .biClrUsed = DEFAULT,
            .biClrImportant = DEFAULT,
    };
    return ans;
}

enum write_status write_header(FILE* out, const struct bmp_header* header) {
    size_t x = fwrite(header, sizeof(struct bmp_header), 1, out);
    if (x == 0) {
        fprintf(stderr, "Cannot write to output file\n");
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum read_status read_header(FILE *in, struct bmp_header *header) {
  uint16_t in16[1];
  uint32_t in32[1];
  size_t x;
  
  x = fread(in16, sizeof(uint16_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  if (in16[0] != SIGNATURE) {
    return READ_INVALID_HEADER;
  }
  header->bfType = in16[0];
  
  x = fread(in32, sizeof(uint32_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->bfileSize = in32[0];
  
  x = fread(in32, sizeof(uint32_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->bfReserved = in32[0];
  if (in32[0]) {
    return READ_INVALID_HEADER;
  }
  
  x = fread(in32, sizeof(uint32_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->bOffBits = in32[0];
  if (in32[0] < 54) {
    return READ_IMAGE_CORRUPTED;
  }
  
  x = fread(in32, sizeof(uint32_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->biSize = in32[0];
  if (in32[0] != HEADER_SIZE) {
    return READ_INVALID_HEADER;
  }
  
  x = fread(in32, sizeof(uint32_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->biWidth = in32[0];
  
  x = fread(in32, sizeof(uint32_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->biHeight = in32[0];
  
  x = fread(in16, sizeof(uint16_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->biPlanes = in16[0];
  if (in16[0] != 1) {
    return READ_INVALID_HEADER;
  }
  
  x = fread(in16, sizeof(uint16_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->biBitCount = in16[0];
  if (in16[0] != 24) {
    return READ_UNSUPORTED_TYPE;
  }
  
  x = fread(in32, sizeof(uint32_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->biCompression = in32[0];
  if (in32[0] != 0 && in32[0] != 1 && in32[0] != 2) {
    return READ_INVALID_HEADER;
  }
  
  x = fread(in32, sizeof(uint32_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->biSizeImage = in32[0];
  
  x = fread(in32, sizeof(uint32_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->biXPelsPerMeter = in32[0];
  
  x = fread(in32, sizeof(uint32_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->biYPelsPerMeter = in32[0];
  
  x = fread(in32, sizeof(uint32_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->biClrUsed = in32[0];
  
  x = fread(in32, sizeof(uint32_t), 1, in);
  if (x == 0) {
    return READ_IMAGE_CORRUPTED;
  }
  header->biClrImportant = in32[0];
  return READ_OK;
}
