#ifndef BMP_READER_AND_WRITER
#define BMP_READER_AND_WRITER

#include "bmpHeader.h"
#include "img.h"

enum read_status check_errors(FILE* in);

size_t get_padding(const struct image* img);

enum read_status read_bmp(FILE* in, struct image* img);

enum read_status from_bmp( FILE* in, struct image* img);

enum write_status write_bmp(FILE* out, const struct image* img);

enum write_status to_bmp( FILE* out, struct image const* img );

#endif
