#ifndef READER
#define READER
#include "bmpHeader.h"
#include "bmpReaderAndWriter.h"
#include "img.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum compatibility_error {
    COMP_OK,
    NOT_SUPPORTED_TYPE,
    INCORRECT_SIGNATURE,
    FILE_OPENING_ERROR,
    UNKNOWN_READING_ERROR
};

enum process_result {
    SUCCESS,
    POINTER_ERROR
};

struct compatibility_result {
    enum image_type type;
    enum compatibility_error err;
    FILE* ptr;
};

FILE* open_input_file(const char* inputFile);
struct compatibility_result check_compatibility(const char* filename);

enum process_result process(FILE* in, FILE* out, enum image_type type);

#endif
