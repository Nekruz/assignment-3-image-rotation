#ifndef IMAGE_STRUCT
#define IMAGE_STRUCT

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

enum image_type {
  BMP,
  UNKNOWN_TYPE
};

struct pixel { uint8_t b, g, r; };

struct image {
  uint32_t width;
	uint32_t height;
  struct pixel* data;
};

int rotate(struct image* img);

void destroy_img(struct image* img);

#endif
