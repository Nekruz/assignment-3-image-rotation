#ifndef BMP_HEADER
#define BMP_HEADER
#include "img.h"
#include  <stdint.h>
#include <stdio.h>
#define SIGNATURE 0x4D42
#define HEADER_SIZE 40
#define BIT_COUNT 24
#define PLANES 1
#define DEFAULT 0

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_OUT_OF_MEMORY,
  READ_IMAGE_CORRUPTED,
  READ_UNSUPORTED_TYPE
  /* коды других ошибок  */
  };

/*  serializer   */
enum  write_status {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};

enum read_status read_header(FILE* in, struct bmp_header* header);

enum write_status write_header(FILE* out, const struct bmp_header* header);

struct bmp_header build_header_from_img(const struct image* img);

size_t get_image_size(const struct image* img);

#endif
