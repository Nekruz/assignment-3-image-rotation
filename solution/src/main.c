#include "include/util.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc < 3) {
        fprintf(stderr, "Expected 2 arguments %i given", argc);
        return 1;
    }
    int error_code = 0;
    FILE* in = NULL;
    FILE* out = NULL;
    struct compatibility_result temp = check_compatibility(argv[1]);
    in = temp.ptr;
    if (temp.err != COMP_OK) {
        error_code = 1;
        goto return_exit;
    }
    out = fopen(argv[2], "wb");
    process(in, out, temp.type);
    goto return_exit;

return_exit:
    if (in != NULL) {
        fclose(in);
    }
    if (out != NULL)  {
        fclose(out);
    }
    return error_code;
}
